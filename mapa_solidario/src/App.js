import React from 'react'
import './global.css'
import SideBar from './SideBar';
import MapContainer from './Map';

function App(){
    return (
      <div className="container">

        <SideBar />
        <MapContainer />

      </div>
  );
     
       
   
}

export default App