import React, { useState } from 'react';
import './style.css';

export default function SideBar() {

  const [name, setName] = useState('')

  return (
    <div className="content">
      <h2>Mapa Solidário</h2>

      <form>
        <input
          placeholder="Nome Completo"
          value={name}
          onChange={e => setName(e.target.value)} />

        <input placeholder="Telefone" />

        <input placeholder="Endereço" />

        <input placeholder="Necessidade" />
        
        <button>Enviar</button>

      </form>

    </div>

  );

}

