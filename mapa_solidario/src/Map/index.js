import React from 'react';
import { Map, GoogleApiWrapper } from 'google-maps-react';

export function MapContainer() {
  
  return (
      <Map 
        google={this.props.google}
        zoom={13}
        style={mapStyles}
        initialCenter={{ lat: -24.7136100, lng: -53.7430600}}
      />
  );
}

const mapStyles = {
   
   
    flex: 1  
    
};
  
export default GoogleApiWrapper({
  apiKey: 'AIzaSyBsPIe0y7l6QW8uslcU6P7zKHN_1GjxRQI'
})(MapContainer);